import { SimulationApp } from './app/App';

const canvas = document.createElement("canvas");
canvas.width  = 512; //Instead of window.innerWidth
canvas.height = 512; //Instead of window.innerHeight
document.body.appendChild(canvas);

const app_instance = new SimulationApp(canvas, 2);
//Set things running
setInterval(() =>
{
    app_instance.update();
    app_instance.draw();
}, 50)