export default class Shader {
    public program: WebGLProgram;
    public gl_context: WebGL2RenderingContext;

    constructor(gl_context: WebGL2RenderingContext, vertex_source: string, fragment_source: string)
    {
        //Store context, place in const for readability
        const gl = this.gl_context = gl_context;

        //Create a program and store it
        var prog = gl_context.createProgram();
        if (!prog)
        {
            throw console.error("Failed to create program");
        }
        else
        {
            this.program = prog;
        }

        var vshader = this.createGLShader(vertex_source, gl.VERTEX_SHADER);
        var fshader = this.createGLShader(fragment_source, gl.FRAGMENT_SHADER);
        gl.attachShader(prog, vshader);
        gl.deleteShader(vshader);
        gl.attachShader(prog, fshader);
        gl.deleteShader(fshader);
        gl.linkProgram(prog);

        //Handle errors
        var error = gl.getProgramInfoLog(prog);
        if (error) { console.log("Problem with program creation: " + error); }
        else { console.log("Program creation succeeded"); }
    }

    private createGLShader(shader_source: string, shader_type: number): WebGLShader
    {
        const gl = this.gl_context; //Convenience variable
        var shader = gl.createShader(shader_type);
        if (!shader) { throw console.error("Failed to create shader of type " + shader_type); }

        gl.shaderSource(shader, shader_source);
        gl.compileShader(shader);

        //Handle errors
        var error = gl.getShaderInfoLog(shader);
        if (error) { console.log("Shader compiler error: " + error); }
        else { console.log("Shader compiled successfully"); }


        return shader;
    }

    public uniform2f(uniform_name: string, x: number, y: number): boolean
    {
        this.use();        
        var location = this.gl_context.getUniformLocation(this.program, uniform_name);
        if (location == -1)
        {
            this.disable();
            return false;
        }

        this.gl_context.uniform2f(location, x, y);
        this.disable();
        return true;
    }

    public use(): void
    {
        this.gl_context.useProgram(this.program);
    }

    public disable(): void
    {
        this.gl_context.useProgram(null);
    }

    public delete(): void
    {
        this.disable();
        this.gl_context.deleteProgram(this.program);
    }
}