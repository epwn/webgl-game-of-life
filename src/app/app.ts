import { readFileSync } from "fs";
import Shader from "./Shader";

export class SimulationApp
{
    public SimulationCanvas: HTMLCanvasElement

    private mGL: WebGL2RenderingContext;

    private mWidth: number;
    private mHeight: number;
    private mWidth_Cells: number;
    private mHeight_Cells: number;

    private mCurrentFBIndex: number;
    private mFBTextures: WebGLTexture[];
    private mFrameBuffer: WebGLFramebuffer;
    private mVAO: WebGLVertexArrayObject;

    private mDrawShader: Shader;
    private mUpdateShader: Shader;

    constructor(canvas: HTMLCanvasElement, side_length: number) {
        this.GenerateBoundaries(canvas, side_length);

        //Grab GL context and shader source
        const gl = this.mGL = this.getWebGLContext(canvas);
        //Initialize variables
        this.GenerateFramebufferTextures();
        this.mCurrentFBIndex = 0; //Currently using mFBTextures[0]
        this.mFrameBuffer = gl.createFramebuffer();
        gl.bindFramebuffer(gl.FRAMEBUFFER, this.mFrameBuffer);

        //Initialize Shaders
        this.mUpdateShader = new Shader(gl,
            readFileSync("./src/app/shaders/UpdateVertex.glsl", "utf-8"),
            readFileSync("./src/app/shaders/UpdateFragment.glsl", "utf-8"));
        this.mDrawShader = new Shader(gl, 
            readFileSync("./src/app/shaders/DrawVertex.glsl", "utf-8"),
            readFileSync("./src/app/shaders/DrawFragment.glsl", "utf-8"));

        //Bind draw shader
        this.mDrawShader.use();
        //Create vertex array and bind it
        this.mVAO = gl.createVertexArray();
        gl.bindVertexArray(this.mVAO);

        //Create quad vertices
        var vertices = new Float32Array([
            -1, -1,
            -1,  1,
             1, -1,
             1,  1
        ]);
        var vertex_pos_buffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vertex_pos_buffer);
        gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
        gl.enableVertexAttribArray(0);
        gl.vertexAttribPointer(0, 2, gl.FLOAT, false, 0, 0);

        //Draw initial state
        this.draw();
    }

    public update(): void
    {
        const gl = this.mGL;
        //Bind state framebuffer, set viewport to state texture size
        gl.bindFramebuffer(gl.FRAMEBUFFER, this.mFrameBuffer);
        gl.viewport(0, 0, this.mWidth_Cells, this.mHeight_Cells);
        //Grab textures
        const next_index = this.mCurrentFBIndex ^ 1; //Bitwise XOR cycles 0 and 1
        var current_texture = this.mFBTextures[this.mCurrentFBIndex];
        var next_texture = this.mFBTextures[next_index];
        //current_texture used as state, next_texture used as frame buffer
        gl.bindTexture(gl.TEXTURE_2D, current_texture);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, next_texture, 0);
        //Bind scale uniform
        this.mUpdateShader.uniform2f("u_scale", this.mWidth_Cells, this.mHeight_Cells);
        //Clear
        gl.clearColor(0.0, 0.0, 0.0, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT);
        //Bind update shader, draw call
        this.mUpdateShader.use();
        gl.bindVertexArray(this.mVAO);
        gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);

        //Advance index
        this.mCurrentFBIndex = next_index;
    }

    public draw(): void
    {
        const gl = this.mGL;
        //Use default framebuffer for screen out, set viewport to canvas size
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        gl.viewport(0, 0, this.mWidth, this.mHeight);
        //Bind current state texture
        gl.bindTexture(gl.TEXTURE_2D, this.mFBTextures[this.mCurrentFBIndex]);
        //Clear
        gl.clearColor(0.0, 0.0, 0.0, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT);
        //Bind draw shader, draw call
        this.mDrawShader.use();
        gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
    }

    private GenerateBoundaries(canvas: HTMLCanvasElement, side_length: number): void
    {
        //Set width/height in pixels
        this.mWidth = canvas.width;
        this.mHeight = canvas.height;

        //Set width/height in cells
        this.mWidth_Cells = Math.round(this.mWidth / side_length);
        this.mHeight_Cells = Math.round(this.mHeight / side_length);
    }

    private GenerateFramebufferTextures(): void
    {
        const gl = this.mGL;
        this.mFBTextures = [null, null];
        for (let i = 0; i < 2; i++)
        {
            this.mFBTextures[i] = gl.createTexture();
            var pixel_count = this.mWidth_Cells * this.mHeight_Cells;
            var texture_array = this.RandomizedTextureArray(pixel_count, 0.5);

            if (i == 1)
            {
                for (let j = 0; j < texture_array.length; j++)
                {
                    texture_array[j] = 255;
                }
            }

            gl.bindTexture(gl.TEXTURE_2D, this.mFBTextures[i]);
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, this.mWidth_Cells, this.mHeight_Cells, 0, gl.RGBA, gl.UNSIGNED_BYTE, texture_array);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        }
    }

    private RandomizedTextureArray(pixels: number, fraction_alive: number = 0.5): Uint8Array
    {
        var tex_array = new Uint8Array(4 * pixels);
        for (let i = 0; i < tex_array.length; i += 4)
        {
            var value = Math.random() < fraction_alive ? 255 : 0;
            tex_array[i]   = value;
            tex_array[i+1] = value;
            tex_array[i+2] = value;
            tex_array[i+3] = 255;
        }

        return tex_array;
    }

    private getWebGLContext(canvas: HTMLCanvasElement): WebGL2RenderingContext
    {
        var gl = canvas.getContext('webgl2', { antialias: false });
        var isWebGL2 = !!gl;
        if(!isWebGL2 || !gl) { throw console.error("No WebGL2 context"); }
        
        return gl;
    }
}